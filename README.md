# Silver's wireguard setup
Rough steps to setup a wireguard server and client

[[_TOC_]]

# Preface

I am pulling info from a number of tutorials, cutting out fluff etc

* https://www.wireguard.com/quickstart/
* https://www.ckn.io/blog/2017/11/14/wireguard-vpn-typical-setup/
* https://www.linode.com/docs/networking/vpn/set-up-wireguard-vpn-on-ubuntu/
* https://wiki.debian.org/Wireguard
* https://wiki.archlinux.org/index.php/WireGuard

I am also running all commands as root user, commands will be as such

# Install
## Main wireguard program
Install as per https://www.wireguard.com/install/

## QR code generator
install ``qrencode`` (if linux) via your normal method

## Enable IP forwarding
Run as root or with sudo

```shell
nano /etc/sysctl.conf
```
```ini
# change
#net.ipv4.ip_forward=1

# to
net.ipv4.ip_forward=1

# also option for ipv6 if you need that
```

update sysctl
```shell
sysctl -p /etc/sysctl.conf
```

# Config Server
## Run as root

```shell
# Make sure ye are root
sudo -i
```

## Basic setup

```shell

# get into the correct folder
cd /etc/wireguard

# Creating folders just for organisation
mkdir config
mkdir config/server
mkdir config/clients
```

## Generate keys
```shell
umask 077
# generate server key and pop it in the config/server folder
wg genkey | tee config/server/wg0_private | wg pubkey > config/server/wg0_public

# also generate first peer here, 001 as the subnetting below can go up to 255 IPs
wg genkey | tee config/clients/001_private | wg pubkey > config/clients/001_public
```

## wg0.conf
I personally like using the 10.0.0.0 ip block as its easy to remember and its distinct from the usual 192.168.1.0 ip block that most home networks use.  
For the server you are assigning a subnet of 10.0.0.0 to 10.0.0.255  
Setting the server as the 10.0.0.0 ip as its the initial peer, and I like zero indexing.  
Note the peer has a /32, this indicates that it gets this specific IP, the /24 for the interface/server aloows it to interface with a range of IP's

Ye should be able to see why I generated the first peer pub/private keys, it makes it easier on first run setup.

```shell
nano config/server/wg0.conf
```

```ini
[Interface]
Address = 10.0.0.0/24
ListenPort = <server port>
PrivateKey = <server private key>

[Peer]
# 001 - <optional name here for reference>
PublicKey = <client public key>
AllowedIPs = 10.0.0.1/32
```

Use [ctrl]+[x] to save

## Managing the interface

Most tutorials will edit the ``wg0.conf`` in the root folder directly, but I found out the hard way that you cannot update this file while the interface is up.  
So now I have the master copy at ``config/server/wg0.conf``, exit that and when I am ready I bring down the interface, copy over the master and bring the interface back up


To start the ball rolling I do:
```shell
cp config/server/wg0.conf wg0.conf
wg-quick up wg0
```

For any changes to the master copy I use:
```shell
wg-quick down wg0
cp config/server/wg0.conf wg0.conf
wg-quick up wg0
```


# Config Client
## Config file
Client config is much like the servers.  
Adding ``DNS = <DNS IP>`` under ``[Interface]`` will set a dns server for the client.  
``AllowedIPs`` sets what traffic goers through the tunnel, ``0.0.0.0/0`` will redirect all ipv4 traffic thropugh it, adding ``, ::/0`` will also redirect all ipv6 traffic

```shell
nano config/clients/001.conf
```
```ini
[Interface]
PrivateKey = <client private key>
Address = 10.0.0.1/32

[Peer]
PublicKey = <server public key>
Endpoint = <public IP of router>:<router port>
AllowedIPs = 0.0.0.0/0
```

## Using the config file
Now grab this file off the server and import it into the woreguard client, should be fairly straight forward.

## Using the config file - Mobile
Remember ``qrencode`` I asked you to install earlier?  
Now do:
```shell
qrencode -t ansiutf8 < config/clients/001.conf
```
This will generate a nice QR code that mobile clients can scan to import the config.

## Generateing for the 2nd client
```shell
# 2nd client
umask 077
wg genkey | tee config/clients/002_private | wg pubkey > config/clients/002_public
```
### Server Config:
```ini
[Interface]
Address = 10.0.0.0/24
ListenPort = <server port>
PrivateKey = <server private key>

[Peer]
# 002 - <optional name here for reference>
PublicKey = <client public key>
AllowedIPs = 10.0.0.2/32
```

### Client Config
```shell
nano config/clients/002.conf
```
```ini
[Interface]
PrivateKey = <client private key>
Address = 10.0.0.2/32

[Peer]
PublicKey = <server public key>
Endpoint = <public IP of router>:<router port>
AllowedIPs = 0.0.0.0/0
```

# Config Router
This will vary depending on what router you have, as such I cant give specific instructions.  
But basically you will forward ``<router port>`` to ``<server port>`` at ``<server ip>`` where the server IP is the IP of the server in your network.   
This is the address that ye get when you run ``ip a`` and look at your main connection (normally wlan0 or eth0), you may want to make this a static adress on the router, elsewise it could get messed up later when the initial ip lease is up.

# Test Setup
Easiest way to test is with a mobile device.

1. Set up the config on the mobile.
2. Turn off wifi
3. Get current IP: https://www.google.com/search?q=what+is+my+ip
4. Turn on the VPN
5. Get IP again
   * If it changed then the VPN is working!
       * Go to [Dynamic IP/DNS](#Dynamic-IP/DNS) to tidy it all up
   * If it did not change then somethign went wrong in the config, 
       1. Did you set the correct ``<public IP of router>``? 
       2. Did you set up the port forwarding?
       3. Does your server have a static IP within the network?
       4. You may need to open the server's firewall, on my fresh install of Debian 10 I didnt have to but you may have to.
           * See the ``PostUp`` and ``PostDown`` fields in the ``wg0.conf`` from the linode tutorial listed above.

# Dynamic IP/DNS
Now in the client configs above you set the ``<public IP of router>``.  
This would work well in the case of a business internet connection where you have a static IP and will work fine for testing the setup on a home network.  
If you know you have a static IP you can stop rading here.

## Most folks
However with a home network you are not guarenteed to have a static IP.  
The solution is DynamicDNS, basically a script/tool runs on yer server and updates a dns entry that you can point the config to.  
For most folks I would recommend using https://www.duckdns.org/ as its very easy to set up.  
With them you will get ``<subDomain>.duckdns.org`` which you can replace ``<public IP of router>`` with, that address will always pount to ``<public IP of router>`` and thus allows you to replace it in the config.

## Own domain
If you have your own domain and your dns provider as an api you can run your own DyDNS.  
There is a good chance that someone else has made tools for this. Search for ``https://www.google.com/search?q=ddns+<provider>`` or ``https://www.google.com/search?q=<github/gitlab>+ddns+<provider>``

### Self Shilling
If you need one for cloudflare I created a typescript script for it: https://gitlab.com/Silvers_General_Stuff/cloudflare_ddns (I really need to do documentation there)
